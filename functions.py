import pygmo as pg
import numpy as np

class G_problems():
    """
    This class encapsulates the G functions. 
    """    
    
    def __init__(self):
        self.problem_set = pg.cec2006
        self.p4 = pg.problem(self.problem_set(prob_id=4))
        self.p6 = pg.problem(self.problem_set(prob_id=6))
        self.p8 = pg.problem(self.problem_set(prob_id=8))
        self.p9 = pg.problem(self.problem_set(prob_id=9))
        self.p19 = pg.problem(self.problem_set(prob_id=19))
        self.p24 = pg.problem(self.problem_set(prob_id=24))            

    def G4(self, x):
        response = self.p4.fitness(x)[1:]
        return np.array(response)
        
    def G6(self, x):
        response = self.p6.fitness(x)[1:]
        return np.array(response)
        
    def G8(self, x):
        response = self.p8.fitness(x)[1:]
        return np.array(response)
        
    def G9(self, x):
        response = self.p9.fitness(x)[1:]
        return np.array(response)
        
    def G19(self, x):
        response = self.p19.fitness(x)[1:]
        return np.array(response)
        
    def G24(self, x):
        response = self.p24.fitness(x)[1:]
        return np.array(response)

