# supporting methods

# imports
import numpy as np
import xml.etree.ElementTree as etree
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
import matplotlib.colors as mc

plt.ion()
# CSV-Numpy operations
def save_to_csv(filename, numpy_array):
    np.savetxt(filename, numpy_array, delimiter=',')
    print('Array saved in: ', filename)
    
def load_from_csv(filename):
    numpy_array = np.genfromtxt(filename, delimiter=',')
    print('Initial design loaded from: ', filename)
    return numpy_array
    
def load_from_text(filename):
    f = open(filename, 'r')
    text = f.readlines()
    f.close()
    a = []
    for i in text:
        a.append(np.array(i.split(), dtype="float"))
    return np.array(a)
    
def extract_model_tags(model):
    tags = [name for name in model.parameter_names_flat()]
    tags.append(model.parameter_names()[-1])
    return tags
    
def extract_model_params(model):
    return model.param_array.tolist()
    
def counted(fn):
    def wrapper(*args, **kwargs):
        wrapper.called+= 1
        return fn(*args, **kwargs)
    wrapper.called= 0
    wrapper.__name__= fn.__name__
    return wrapper
    
def read_settings(xml_file_name):
    tree = etree.parse(xml_file_name)
    
def plot_contour_2D(func, lb, ub, delta=0.01, nlevels=10, args=(), kwargs={}, \
    ctitle="Function Values", data=None):
    """
    Plot contour lines of the function outputs. 
    
    Parameters.
    -----------
    func (method): the function that we are interested in
    lb (np.array): lower bounds
    ub (np.array): upper bounds
    delta (float): step size in each dimension
    nlevels (int): number of contour levels.
    args (tuple): arguments to the function.
    kwargs (dictionary): keyword arguments to the function.
    ctitle (str): colourbar title.
    data (m x 2 numpy array): data to scatter on top of the contour plot.
    
    Draws a contour plot.
    """
    x = np.arange(lb[0], ub[0]+0.001, delta)
    y = np.arange(lb[1], ub[1]+0.001, delta)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros(X.shape)
    for (i, j), val in np.ndenumerate(Z):
        param = np.array([X[i,j], Y[i,j]]).reshape((1,-1))
        try:
            Z[i,j] = func(param, *args, **kwargs)
        except:
            Z[i,j] = func(param[0], *args, **kwargs)
    vmin = np.min(Z)
    vmax = np.max(Z)
    try:
        levels = np.arange(vmin, vmax + ((vmax- vmin) / nlevels) +1e-8, (vmax - vmin)/nlevels)
        norm = mc.BoundaryNorm(levels, 256)
        CS1 = plt.contourf(X, Y, Z, levels=levels, norm=norm, alpha=0.75)
    except Exception as e:
        print(e)
        print("Failed to do many levesls.")
        CS1 = plt.contourf(X, Y, Z, alpha=0.75)
    cbar = plt.colorbar(orientation='horizontal', pad=0.15)
    tick_locator = ticker.MaxNLocator(nbins=6)
    cbar.locator = tick_locator
    cbar.formatter.set_scientific(True)
    cbar.formatter.set_powerlimits((-1, 1))
    cbar.update_ticks()
    cbar.set_label(ctitle)
    if data is not None:
        plt.scatter(data[:,0], data[:,1], marker="s", color="red", s=80)
    plt.xlabel('$x_0$')
    plt.ylabel('$x_1$') 
    plt.xlim([lb[0], ub[0]])
    plt.ylim([lb[1], ub[1]])
    plt.draw()
    
def feas_prob(history):
    pf = np.sum(history[:,1,:], axis=1)/ (np.sum(history[:,1,:], axis=1) + np.sum(history[:,0,:], axis=1))
    pinf = 1 - pf
    tpr = history[:,:,1][:,1]/np.sum(history[:,1,:], axis=1)
    fpr = history[:,0,:][:,1] /np.sum(history[:,0,:], axis=1)
    prob = (tpr * pf)/((tpr * pf) + (fpr * pinf))
    inds = np.where(tpr == 0)[0]
    prob[inds] = 0
    if np.isnan(prob).any():
        import pdb; pdb.set_trace()
    return prob
    
    
def informedness(history):
    """
    Remember this:
    confusion_matrix = np.array( [ [TN, FP], [FN, TP] ] )
    """
    condition_positive = np.sum(history[:,1,:], axis=1)  #+ 1e-11
    true_positive = history[:,:,1][:,1]#history[:,:,0][:,0]
    sensitivity = true_positive / condition_positive
    condition_negative = np.sum(history[:,0,:], axis=1) #+ 1e-11
    '''
    if (np.any(condition_negative == 0)) or (np.any(condition_positive == 0)):
        import pdb; pdb.set_trace()
    '''
    true_negative = history[:,:,0][:,0]#history[:,:,1][:,1]
    specificity = true_negative/condition_negative
    informed = sensitivity + specificity -1
    """
    if np.any(informed < 0):
        import pdb; pdb.set_trace() 
    """
    return informed
    
def F1(history):
    """
    Remember this:
    confusion_matrix = np.array( [ [TN, FP], [FN, TP] ] )

    """
    true_positive = history[:,:,1][:,1]#history[:,:,0][:,0]
    true_negative = history[:,:,0][:,0]#history[:,:,1][:,1]
    false_positive = history[:,:,1][:,0]#history[:,:,0][:,0]
    false_negative = history[:,:,0][:,1]#history[:,:,0][:,0]
    precision = true_positive / (true_positive + false_positive + 1e-11)
    recall = true_positive / (true_positive + false_negative + 1e-11)
    #import pdb; pdb.set_trace()
    f1 = 2 * precision * recall / (precision + recall + 1e-11)
    if np.isnan(np.sum(f1)):
        import pdb; pdb.set_trace()
    return f1
    
    
def step_confusion(X, func, thresholds, model, fargs=(), fkwargs={}, lb=None, ub=None):
    Y = np.array([func(x, *fargs, **fkwargs) for x in X])
    y_true = np.array([np.all((y - thresholds)<=0) for y in Y]) # feasibility is less than zero
    if hasattr(model, 'n_models'):
        y_pred = np.concatenate(model.prob_feasibility(X, thresholds, lb, ub))
        #np.array([model.prob_feasibility(x, thresholds, lb, ub) for x in X])
    else:
        y_pred = []
        for x in X:
            x = np.atleast_2d( x )
            prob_in = model.prob_feasibility(x, 0, lb, ub)[0][0]
            y_pred.append( prob_in )
        y_pred = np.array(y_pred)
    #import pdb; pdb.set_trace()
    y_pred = np.array(y_pred > 0.5)
    #import pdb; pdb.set_trace()
    TP = sum(np.logical_and( y_true, y_pred  ) ) # y_true and y_pred
    FP = sum(np.logical_and( np.logical_not( y_true ), y_pred ) ) # not y_true, but y_pred
    FN = sum(np.logical_and( y_true, np.logical_not( y_pred ) ) ) # y_true, but not y_pred
    TN = sum(np.logical_and( np.logical_not( y_true ), np.logical_not( y_pred ) ) ) # not y_true and not y_pred
    confusion_matrix = np.array( [ [TN, FP], [FN, TP] ] )
    #import pdb; pdb.set_trace()
    return( confusion_matrix )
    
    
    
    
    
