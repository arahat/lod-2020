import numpy as np
# import pickle
import IscaOpt
from scipy.stats import norm
import mono_funcs as mf

class gp_active_learn():

    def __init__(self, function, args, settings):
        # simple function to initialise the settings
        self.function = function
        self.args = args
        self.settings = settings
        self.thresholds = self.settings['ref_vector']
        self.lb = self.settings['lb']
        self.ub = self.settings['ub']
        
    def fit(self, type_learn = 'multi'):
        """
        Function for fitting

        X: Training set
        Y: Training set ouptut
        mop: Model output information

        # Alma's code, the following return a probaility:
        mop.models.excursion_probability() - For multi surrogates
        mop.surr.excursion_probability() - For mono surrogates
        """
        self.history = None
        if type_learn == 'multi':
             # TODO - need to make sure the restarts = 9 again
             # create the optimised models
             X, Y, hist, mop = IscaOpt.Optimiser.EMO(
                                        self.function, \
                                        fargs = self.args, \
                                        settings = self.settings, \
                                        )
        elif type_learn == 'mono':
            #X = None # TODO need to adjust the function so it outputs X
            # TODO need to make the restarts = 9 before main run
            mop, X, Y, hist = mf.gp_build_multi_threshold( \
                                             fun = self.function, \
                                             fargs = self.args, \
                                             settings = self.settings, \
                                          )
        self.history = hist
        self.type_learn = type_learn
        self.X = X
        self.Y = Y
        self.mop = mop # only needed for alma's function
        self.n_dim = self.settings['n_dim']

    def predict_proba(self, X_new):
        """
        TODO This function needs to be completed
        """
        if self.n_dim == X_new.shape[1]:
            Y_hat_prob = [ ]
            for x in X_new:
                # predict
                if self.type_learn == 'multi':
                    prob = self.mop.models.prob_feasibility( \
                                                x, \
                                                self.mop.thres, \
                                                self.mop.lower_bounds, \
                                                self.mop.upper_bounds, \
                                                    )
                    prob = np.asscalar( prob )
                    Y_hat_prob.append( prob )
                elif self.type_learn == 'mono':
                    # TODO wrap the prob calculation in 'excusion probability'
                    x = np.atleast_2d( x )
                    prob_in = mop.prob_feasibility(x, 0, self.lb, self.ub)
                    Y_hat_prob.append( prob_in )
        else:
            print( 'X_new has the wrong number of dimensions')
            return None
        return np.array( Y_hat_prob )

    def excurs_predict(self, X_new):
        prob = self.predict_proba(X_new)
        in_excur_hat = (prob > 0.5)
        return( in_excur_hat )

    def true_out(self, X_new):
        if self.n_dim == X_new.shape[1]:
            Y = [ ]
            for x in X_new:
                if self.args != ():
                    # check the number of input arguments to the function
                    if len(self.args) == 2:
                        Y.append( self.function(x, self.args[0], self.args[1]) )
                    if len(self.args) == 1:
                        Y.append( self.function(x, self.args[0] ) )
                else:
                    Y.append( self.function( x ) )

            return( np.array( Y ) )
        else:
            return None

    def excurs( self, X_new):
        y_abs = self.true_out( X_new )
        self.Y_excusion_samples = y_abs
        self.X_excusion_samples = X_new
        in_excur = [ ]
        for y in y_abs:
            # feasible set is the excursion set (greater than both probabilities)
            diff = y - self.settings['ref_vector']
            in_excur.append( (diff <= 0).all() )
        # return a vector of t/f
        return( np.array( in_excur ) )

    def confusion_old(self, X):
        """
        Takes in the matrix of n input vectors (X)
        and their true values Y (true of false) and
        gives them a score
        """
        Y_ex = self.excurs(X) # This may need some tweaking
        #import pdb; pdb.set_trace()
        Y_hat_ex = self.excurs_predict(X)
        TP = sum(np.logical_and( Y_ex, Y_hat_ex  ) )
        FP = sum(np.logical_and( np.logical_not( Y_ex ), Y_hat_ex ) )
        FN = sum(np.logical_and( Y_ex, np.logical_not( Y_hat_ex ) ) )
        TN = sum(np.logical_and( np.logical_not( Y_ex ), np.logical_not( Y_hat_ex ) ) )
        confusion_matrix = np.array( [ [TN, FP], [FN, TP] ] )

        return( confusion_matrix )
        
    def confusion(self, X):
        func = self.function
        fargs = self.args
        fkwargs = {}
        Y = np.array([func(x, *fargs, **fkwargs) for x in X])
        y_true = np.array([np.all((y - self.thresholds)<=0) for y in Y]) # feasibility is less than zero
        #import pdb; pdb.set_trace()
        if hasattr(self.mop, 'models'):
            y_pred = np.concatenate(self.mop.models.prob_feasibility(X, self.thresholds, self.lb, self.ub))
        
            #np.array([self.mop.models.prob_feasibility(x, self.thresholds, self.lb, self.ub) for x in X])
        else:
            y_pred = []
            for x in X:
                x = np.atleast_2d( x )
                prob_in = self.mop.prob_feasibility(x, 0, self.lb, self.ub)[0][0]
                y_pred.append( prob_in )
            y_pred = np.array(y_pred)
        #import pdb; pdb.set_trace()
        y_pred = np.array(y_pred > 0.5)
        #import pdb; pdb.set_trace()
        TP = sum(np.logical_and( y_true, y_pred  ) ) # y_true and y_pred
        FP = sum(np.logical_and( np.logical_not( y_true ), y_pred ) ) # not y_true, but y_pred
        FN = sum(np.logical_and( y_true, np.logical_not( y_pred ) ) ) # y_true, but not y_pred
        TN = sum(np.logical_and( np.logical_not( y_true ), np.logical_not( y_pred ) ) ) # not y_true and not y_pred
        confusion_matrix = np.array( [ [TN, FP], [FN, TP] ] )
        #import pdb; pdb.set_trace()
        return( confusion_matrix )
    
        
